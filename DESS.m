(* ::Package:: *)

If[Not[ValueQ[DESSExpansionVariable]],
    DESSExpansionVariable = \[Epsilon];
];

If[Not[ValueQ[DESSExpansionLimit]],
    DESSExpansionLimit = Infinity;
];

(*
 *  for a given rdata, a value x, order of expansion oe and a numerical
 *  precision np, sums up the corresponding series
 *)


EvaluateSeries[rdata_, x_, oe_, np_, sign_:0] := Module[{xn = SetPrecision[x,np], c, n, shift, sdata, coefsN, nnn, sf = Identity(*Factor*), toxseries = Identity, res,oldres,cf},

    (*ClearSystemCache[];*) (* to avoid slowdown due to previous expansions in other variables *)

    sdata = Plus @@ If[TrueQ[Parallel /. Options[DESS]],ParallelMap,Map][Module[{lp, llp, M, coefs, init, input = ##},
        {lp, llp, M, coefs, init} = input;
        (*Print[DESSExpansionVariable];*)
        (*Print[Timing[*)
            coefsN = Series[coefs[nnn],{DESSExpansionVariable,0,oe}];
        (*)]];*)
        shift = Max @@ Exponent[Normal[coefsN], 1/DESSExpansionVariable];
        shift = Max[shift, 0];
        (*Print[shift];*)
        c[0] = SetPrecision[init, np] + SeriesData[DESSExpansionVariable, 0, {}, oe + shift, oe + shift, 1];
        res = c[0];
        For[n = 1, n <= DESSExpansionLimit, ++n,
            (*Print[Timing[*)
                cf = Quiet[
                    Check[
                        coefsN /. nnn->n,
                        (*Print["Fallback for ",n];*)
                        Series[coefs[n],{DESSExpansionVariable,0,oe}]
                    ]
                ];
            (*)]]*)
            cf = SetPrecision[cf,np];
            (*Print[Timing[*)
                c[n] = sf @ Sum[sf[cf[[m]].c[n - m]], {m, Min[M, n]}];
            (*)]];*)
            oldres = res;
            res+=c[n]*xn^n;
            If[Normal[oldres] === Normal[res],
                Print["Stopped after ",n," terms"];
                Break[];
            ];
        ];
        temp = (xn + sign * I * 10^(-np-2))^lp*((Log[xn + sign * I * 10^(-np-2)]^#/#!)&/@Range[0, llp]).Partition[res, Length[res] / (llp + 1)];
        temp
        ]&, rdata, {1}];
    sdata
]


ConstructSeries[rdata_, x_, oe_, np_, terms_] := Module[{c, n, sdata, sf = Identity(*Factor*), toxseries = Identity, res},
    sdata = Join @@ (Function[{lp, llp, M, coefs, init},
        c[0] = N[init,np] + SeriesData[DESSExpansionVariable, 0, {}, oe, oe, 1];
        res = {}; (* silly, but to have same form*)
        For[n = 0, n <= terms - (lp /. {DESSExpansionVariable->0}), ++n,
            If[n > 0, c[n] = sf @ Sum[sf[coefs[n][[m]].c[n - m]], {m, Min[M, n]}];];
            res = Join[res, (Transpose[{((x^(n+lp)*Log[x]^#/#!) & /@ Range[0, llp]), Transpose[Partition[c[n], llp + 1]]}])  ];
        ];
        res
        ] @@@ rdata);
    sdata
]

(*
 * the linear-fractional transformation which maps {k, l, m}
 * to (-1,0,1)
 *)

FindLFT[x_, {k_, l_, m_}] := Module[{eqs, fun, res, a, b, c, d},
    eqs = Function[{number, goal},
        If[number === Infinity,
            a == goal*c,
            a*number + b == goal*(c*number + d)
        ]
    ] @@@ {{k, -1}, {l, 0}, {m, 1}};
    res = Solve[Append[eqs, a*d - b*c == 1], {a, b, c, d}][[1]];
    {a, b, c, d} = {a, b, c, d} /. res;
    fun = Together[(a*x + b)/(c*x + d)];
    fun
]


InverseLFT[x_, f_] := Module[{temp},
    Off[Power::infy];
    temp = {f /. {x -> -1}, f /. {x -> 0}, f /. {x -> 1}};
    temp = temp /. {ComplexInfinity -> Infinity};
    On[Power::infy];
    temp = FindLFT[x, temp];
    temp
]

(*
 *  finds the matching point x with |f1[x]|=|f2[x]| such that x
 *  is between f1^-1[0] and f2^-1[0]
 *)

FindMPoint[x, {f1_, f2_}] := Module[{roots, prz1, prz2},
    roots = Join[x /. Solve[f1 == f2, {x}], x /. Solve[f1 == -f2, {x}]];
    roots = Select[roots, (Abs[f1 /. x -> ##] == Abs[f2 /. x -> ##]) &];
    roots = Select[roots, (Head[N[##]] =!= Complex) &];
    prz1 = x /. Solve[f1 == 0, x];
    If[prz1 == x, prz1 = {Infinity}];
    prz2 = x /. Solve[f2 == 0, x];
    prz1 = First[prz1];
    If[prz2 == x, prz2 = {Infinity}];
    prz2 = First[prz2];
    If[prz1 === Infinity,
        roots = Select[roots,(##<prz2)&];
    ,
        roots = Select[roots, And[prz1 < ##, ## < prz2] &];
    ];
    roots[[1]]
]


(*
 *  rdatas is a list of pairs {f,rdata} corresponding to singular points of DE;
 *  rdatasI if existing is the similar set for inverse functions
 *  Each f is found with FindLFT for a current singular point and its
 *  neigbours;
 *  x is the current variable;
 *  x0 is the point of evaluation
 *)


 RecollectXPowers[f_, x_] := Module[{temp},
   temp = {ExpandAll[##[[1]]], ##[[2]]} & /@ f; (* expand x formula*)

    temp = {
       If[Head[##[[1]]] === Plus,
        List @@ ##[[1]], {##[[1]]}], ##[[2]]} & /@ temp; (*   making lists instead of x monomials*)

   temp = {##[[1]], Table[##[[2]], {Length[##[[1]]]}]} & /@ temp; (*   second part repeated *)
   temp = Transpose /@ temp; (*   transpose to be back to pairs {monomial, coeff*)

   temp = Flatten[temp, 1]; (* flatten to have one list *)

   temp = { PowerExpand[##[[1]]], ##[[2]]} & /@ temp; (*
   to collect x powers *)

   temp = {x^Exponent[##[[1]], x]*
        Log[x]^Exponent[##[[1]],
          Log[x]], (##[[1]] /. {Log[x] -> 1} /. {x ->
            1}) * ##[[2]]} & /@ temp; (*    move coefficients to expressions *)

    temp = {##[[1, 1]], Plus @@ ##[[2]]} & /@ Reap[Sow[##[[2]], x[##[[1]]]] & /@ temp, _, List][[2]]; (*group coefficient at equal powers *)

   temp
]


DESS[rdatas_, x_, x0in_, oe_, np_, terms_:0, StartingRegion_:1] := Module[{x0, change, functions, mpoints, i, j, count, res, temp, pairs, pairs2, PositiveDirection, SpecialPoint = False, signs},

    If[TrueQ[NumberQ[N[x0in]]],
        x0 = x0in;
        change = x - x0;
    ,
        If[(x0in/.{x->Infinity}) == 0,
            x0 = Infinity;
        ,
            x0 = First[x /. Solve[x0in == 0, x]];
        ];
        change = x0in;
    ];

    (* we assume no negative poles currently *)
    functions = First /@ rdatas;
    mpoints = FindMPoint[x, ##] & /@ Transpose[{functions, RotateLeft[functions, 1]}];
    mpoints = RotateRight[mpoints, 1];
    (*Print[mpoints];*)
    AppendTo[mpoints,mpoints[[1]]];
    pairs = Table[{mpoints[[i]],mpoints[[i+1]]},{i,1,Length[mpoints]-1}];
    mpoints = Take[mpoints,Length[mpoints]-1];
    pairs2 = Select[pairs,And[##[[1]]<=x0,x0<=##[[2]]]&];

    If[Length[pairs2]==0,
        i=Length[mpoints];
    ,
        i = Position[pairs,pairs2[[1]]][[1,1]];
    ];


    j = StartingRegion;
    count = 0;
    While[j!=i,
        count = count +1;
        j = j+1;
        If[j>Length[mpoints],j=1];
    ];

    If[i!=StartingRegion,
        Print["We need to go to region ",i," from region ",StartingRegion];
        If[count<=Length[mpoints]/2,
            Print["Will have to make glueing in positive direction"];
            PositiveDirection = True;
        ,
            Print["Will have to make glueing in negative direction"];
            PositiveDirection = False;
        ];
    ,
        Print["We are in starting region"];
        PositiveDirection = True;
    ];


    signs = Sign /@ Im /@ First /@ rdatas /. x -> I;

    i = StartingRegion;
    res = {};
    AppendTo[mpoints,mpoints[[1]]];
    While[True,
        Print["Current region is "<>ToString[i]];
        If[Or[
            And[N[mpoints[[i]]] <= x0, x0 <= N[mpoints[[i + 1]]]]
            ,
            And[i==Length[mpoints]-1,Or[N[mpoints[[i]]] <= x0, x0 <= N[mpoints[[i + 1]]]]]
            ],
            Print["Region found"]
            Break[];
        ];
        If[PositiveDirection,
            j = i + 1;
            If[j==Length[mpoints], j = 1];
            Print["Have to perform glueing and move higher"];
        ,
            j = i - 1;
            If[j==0, j = Length[mpoints]-1];
            Print["Have to perform glueing and move lower"];
        ];
        Print["Evaluating function "<>ToString[i]<>" in point "<>ToString[mpoints[[If[PositiveDirection,i+1,i]]]]];
        temp = EvaluateSeries[rdatas[[i, 2]], (functions[[i]] /. {x -> mpoints[[If[PositiveDirection,i+1,i]]]}), oe, np, signs[[i]]];
        (*calculating in next mpoint in case of positive direction, in current in negative case *)
        (*Print[Det[temp]];*)
        If[res === {},
            res = temp;
        ,
            res = temp.res;
        ];
        If[Length[rdatas[[j]]]==2,
            Print["Evaluating function "<>ToString[j]<>" in point "<>ToString[mpoints[[If[PositiveDirection,i+1,i]]]]<>" to be inverted"];
            temp = EvaluateSeries[rdatas[[j, 2]], (functions[[j]] /. {x -> mpoints[[If[PositiveDirection,i+1,i]]]}), oe, np, signs[[j]]];
            (*Print[Det[temp]];*)
            res = Inverse[temp].res;
        ,
            Print["Evaluating inverse function "<>ToString[j]<>" in point "<>ToString[mpoints[[If[PositiveDirection,i+1,i]]]]];
            temp = EvaluateSeries[rdatas[[j, 3]], (functions[[j]] /. {x -> mpoints[[If[PositiveDirection,i+1,i]]]}), oe, np, signs[[j]]];
            (*Print[Det[temp]];*)
            res = Transpose[temp].res;
        ];

        i = j;
    ];

    If[(functions[[i]] /. {x -> x0})==0,
        Print["Constructing series for function "<>ToString[i]<>" in point "<>ToString[x0]];
        temp=ConstructSeries[rdatas[[i, 2]], x, oe, np, terms];
        temp = temp /. {x -> functions[[i]]};
        temp = temp /. {x -> InverseLFT[x,change]};
        temp = {Simplify[##[[1]]],##[[2]]}&/@temp;
        SpecialPoint = True;
        temp = {Normal[Series[##[[1]],{x,0,terms}]],##[[2]]}&/@temp;
        temp = RecollectXPowers[temp, x];
        (*temp = temp/.{x->change};*)
        temp = Rule[
            {Exponent[##[[1]], x] /. {DESSExpansionVariable -> 0}, Coefficient[Exponent[##[[1]], x], DESSExpansionVariable], Exponent[##[[1]], Log[x]]}
            , ##[[2]]
            ] & /@ temp;
    ,
        Print["Evaluating function "<>ToString[i]<>" in point "<>ToString[x0]];
        temp=EvaluateSeries[rdatas[[i, 2]], (functions[[i]] /. {x -> x0}), oe, np];
    ];
    If[res === {},
        res = temp;
    ,
        If[SpecialPoint,
            res = Rule[##[[1]],##[[2]].res]&/@temp;
        ,
            res = temp.res;
        ];
    ];
    res
]
